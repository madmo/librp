#include "rp.h"
#include <string.h>
#include <stdarg.h>
#include <stdio.h>

char *RP_strdup(const char *s)
{
	unsigned long length;
	char *result;
	length = strlen(s);
	result = RP_RefMalloc(length + 1);
	if (result)
		strcpy(result, s);
	return result;
}

char *RP_strcat(const char *front, const char *back)
{
	unsigned long length;
	char *result;

	length = strlen(front) + strlen(back) + 1;

	result = RP_RefMalloc(length);
	if (result)
	{
		strcpy(result, front);
		strcat(result, back);
	}

	return result;
}

char *RP_sprintf(const char *format, ...)
{
	int length;
	va_list args;
	char *result;

	va_start(args, format);

	length = vsnprintf(NULL, 0, format, args) + 1;
	if (length > 0)
	{
		result = RP_RefMalloc(length);
		if (result)
		{
			if (vsnprintf(result, length, format, args) < 0)
			{
				RP_Release(result);
				result = NULL;
			}
		}
	}

	va_end(args);

	return result;
}
