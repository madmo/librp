/* Copyright (c) 2013 Moritz Bitsch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * A simple template inspired vector for C
 */

#ifndef _VECTR_H_
#define _VECTR_H_

#include <stdlib.h>
#include <string.h>

#define VECTR_T(type) struct { unsigned long m, n; type *a; }

#define VECTR_Init(v) ((v).n = (v).m = 0, (v).a = NULL)
#define VECTR_Destroy(v) do { if ((v).a) free((v).a); } while (0)

#define VECTR_Clear(v) ((v).n = 0)
#define VECTR_Len(v) ((v).n)
#define VECTR_A(v, i) ((v).a[(i)])

#define VECTR_Push(type, v, x) do {                                  \
	if ((v).n == (v).m) {                                        \
		(v).m = (v).m? (v).m<<1 : 2;                         \
		(v).a = (type*)realloc((v).a, sizeof(type) * (v).m); \
	}                                                            \
	(v).a[(v).n++] = (x);                                        \
} while (0)

#define VECTR_Del(type, v, i) do {                                               \
	if ((i) + 1 != (v).m) {                                                  \
		memmove(&(v).a[i], &(v).a[i+1], ((v).m - (i+1)) * sizeof(type)); \
	}                                                                        \
	(v).n--;                                                                 \
} while(0)

#endif
