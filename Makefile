TARGET=librp.a

SRCS=\
	rp.c \
	rpstr.c \

OBJS=$(SRCS:.c=.o)

all: $(TARGET)

$(TARGET): $(OBJS)
	$(AR) rvc $@ $?
	ranlib $@

install: $(TARGET) rp.h
	mkdir -p $(DESTDIR)/usr/lib
	mkdir -p $(DESTDIR)/usr/include
	cp $(TARGET) $(DESTDIR)/usr/lib
	cp rp.h $(DESTDIR)/usr/include

clean:
	rm -f $(TARGET)
	rm -f $(OBJS)

.PHONY: all install clean
