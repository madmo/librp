/* Copyright (c) 2013 Moritz Bitsch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Memory management helper inspired by the Obj-C NSAutoreleasePool
 */

#ifndef _RP_POOL_H_
#define _RP_POOL_H_

/* Included for size_t */
#include <stdlib.h>

/**
 * Refcounted pointer functions
 */
#define RP_RefMalloc(size) RP_RefMallocEx(size, NULL)
void * RP_RefMallocEx(size_t size, void (*destruct)(void*));
void * RP_WrapPointer(void *ptr, size_t size, void (*destruct)(void*));
void * RP_Retain(void *ptr);
void RP_Release(void *ptr);

/**
 *  Autorelease pool functions
 */
void * RP_Autorelease(void *ptr);
void RP_PoolPush();
void RP_PoolPop();

#define autoreleasepool(code) do { RP_PoolPush(); code; RP_PoolPop(); } while (0)

/**
 * Autoreleased string functions
 */
char *RP_strdup(const char *s);
char *RP_strcat(const char *front, const char *back);
char *RP_sprintf(const char *format, ...);

#endif

