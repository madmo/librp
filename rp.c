/* Copyright (c) 2013 Moritz Bitsch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Memory management helper inspired by the Obj-C NSAutoreleasePool
 */

#include <stdlib.h>
#include <string.h>

#include "rp.h"
#include "vectr.h"

typedef struct RP_Pool RP_Pool;
typedef struct RP_RefPtr RP_RefPtr;

struct RP_RefPtr {
	long refcnt;
	unsigned long index;
	RP_Pool *pPool;
	void (*destruct)(void*);
};

struct RP_Pool {
	VECTR_T(void*) ptrs;
	struct RP_Pool *next;
};

#ifndef USE_LIBTASK_TLS
#ifdef _MSC_VER
#define __thread __declspec(thread)
#endif
__thread RP_Pool *g_RP_Pool = NULL;
#define RELEASE_POOL g_RP_Pool
#else
#include <task.h>
#define RELEASE_POOL (*(RP_Pool **)taskdata())
#endif

#define REFCAST(x) ((RP_RefPtr*)(x - sizeof(RP_RefPtr)))

void * RP_RefMallocEx(size_t size, void (*destruct)(void*))
{
	void *result = malloc(sizeof(RP_RefPtr) + size);
	if (!result)
		return NULL;

	memset(result, 0, sizeof(RP_RefPtr));

	((RP_RefPtr*)result)->destruct = destruct;
	((RP_RefPtr*)result)->refcnt = 1;

	return result+sizeof(RP_RefPtr);
}

void * RP_Retain(void *ptr)
{
	if (ptr)
		REFCAST(ptr)->refcnt++;
	return ptr;
}

void RP_Release(void *ptr)
{
	if (!ptr)
		return;

	REFCAST(ptr)->refcnt--;
	if (REFCAST(ptr)->refcnt == 0)
	{
		if (REFCAST(ptr)->destruct)
		{
			REFCAST(ptr)->destruct(ptr);
		}

		if (REFCAST(ptr)->pPool)
		{
			VECTR_Del(void*, REFCAST(ptr)->pPool->ptrs, REFCAST(ptr)->index);
		}

		free(ptr-sizeof(RP_RefPtr));
	}
}

void * RP_WrapPointer(void *ptr, size_t size, void (*destruct)(void*))
{
	void *result = realloc(ptr, sizeof(RP_RefPtr) + size);
	if (!result)
		return NULL;

	memmove(result+sizeof(RP_RefPtr), result, size);

	memset(result, 0, sizeof(RP_RefPtr));

	((RP_RefPtr*)result)->destruct = destruct;
	((RP_RefPtr*)result)->refcnt = 1;

	return result+sizeof(RP_RefPtr);
}

void * RP_Autorelease(void *ptr)
{
	if (ptr && RELEASE_POOL)
	{
		VECTR_Push(void*, RELEASE_POOL->ptrs, ptr);
		REFCAST(ptr)->pPool = RELEASE_POOL;
		REFCAST(ptr)->index = VECTR_Len(RELEASE_POOL->ptrs) - 1;
	}

	return ptr;
}

void RP_PoolPush()
{
	RP_Pool *pNew = malloc(sizeof(RP_Pool));
	VECTR_Init(pNew->ptrs);
	pNew->next = RELEASE_POOL;

	RELEASE_POOL = pNew;
}

void RP_PoolPop()
{
	unsigned long i;
	RP_Pool *pTemp = RELEASE_POOL;

	for (i = 0; i < VECTR_Len(RELEASE_POOL->ptrs); i++)
	{
		REFCAST(VECTR_A(RELEASE_POOL->ptrs, i))->pPool = NULL;
	}

	for (i = 0; i < VECTR_Len(RELEASE_POOL->ptrs); i++)
	{
		RP_Release(VECTR_A(RELEASE_POOL->ptrs, i));
	}
	VECTR_Destroy(RELEASE_POOL->ptrs);

	RELEASE_POOL = RELEASE_POOL->next;
	free(pTemp);
}
